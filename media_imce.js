(function ($) {
namespace('Drupal.media.imce');

Drupal.media.imce.onload = function (win) {
//we use "modal" dialogs, so we shouldn't bother about multiple IMCE
  win.imce.setSendTo(Drupal.t('Insert file'), Drupal.media.imce.sendto);
//we add a 'cancel' button in order to user can close dialog without select
//with IMCE patch this appear as a white cross in a grey disc else just a blue point
  win.imce.opAdd({name: 'cancel', title: Drupal.t('Cancel'), func: Drupal.media.imce.cancel});
//hide our Submit and Cancel buttons
  $('div#media-tab-media_imce a.button').css('display', 'none');
//set up IMCE two new button
  $('iframe#media-imce-iframe').contents().find('li#op-item-cancel').css('float', 'right')
  $('iframe#media-imce-iframe').contents().find('li#op-item-cancel a').css('background-image', 
    'url(' + Drupal.settings.media.browser.media_imce.CancelImage + ')');
  $('iframe#media-imce-iframe').contents().find('li#op-item-sendto a').css('background-image', 
    'url(' + Drupal.settings.media.browser.media_imce.SendtoImage + ')');
}

Drupal.media.imce.cancel = function () {
  $('a.button.fake-cancel').click();
}

Drupal.media.imce.sendto = function (file, win) {
  var imce = win.imce;
  var fid = null;
  var uri = null;

  if (imce.vars.lastfid) fid = imce.vars.lastfid;
  else  for (fid in imce.selected);
  uri = (imce.conf.scheme + '://' + ( imce.conf.dir == '.' ? '' : imce.conf.dir +'/') ) + fid;
  filename = fid;

  $.ajax({
    url: '?q=media/browser/imce/fid',
    type: 'GET',
    dataType: 'json',
    data: {uri: uri},
    error: function () {alert('Error getting data for IMCE.') },
    success: function(data, status) { 
      Drupal.media.browser.selectedMedia[0] = data;
      $('a.button.fake-ok').click();
    }
  });
}

})(jQuery);
