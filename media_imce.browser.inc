<?php

/**
 * AJAX Callback function to IMCEBrowser get file fid
 */

function media_browser_imce_fid() {
  $params = drupal_get_query_parameters();

  $file = file_uri_to_object(urldecode($params['uri']), TRUE);
  file_save($file);

  drupal_json_output(array(
    'preview' => drupal_render(media_get_thumbnail_preview($file)),
    'fid' => $file->fid,
  ));
  exit();
}
